# fizzbuzz REST API

Test technique Manageo

## Lancer fizzbuzz

- Cloner ce repo
- se placer dans le dossier root du projet 
- Exécuter la commande suivante :

`sur windows`
```shell
.\mvnw spring-boot:run
```

`sur linux`
```shell
./mvnw spring-boot:run
```

## Swagger

La documentation de l'API est diponible dans une ui swagger à l'adresse `http://localhost:8080/swagger-ui.html`

## Tester fizzbuzz

- Pour tester via l'IDE, `s'assurer que l'IDE est configuré à utiliser Junit comme runner par défaut`
- Pour tester en ligne de commande, exécuter la commande suivante à partir du dossier root du projet:

`sur windows`
```shell
.\mvnw test
```

`sur linux`
```shell
./mvnw test
```

## Etapes suivies pour le développement de l'API

Une approche TDD a été suivie pour développer le Controller et le service de cette API

`Le Controller FizzBuzzController`
- Création de la classe de Test FizzBuzzControllerTest

- Ecriture de la méthode testFizzBuzzEndpointAccessibility() qui teste l’accéssibilité de /api/fizzbuzz?fizzbuzznumber=10. 
Echec du test DONC création du controller et de la route /api/fizzbuzz pour faire passer le test.

- Ecriture de la méthode testFizzBuzzEndpointReturnFormat() qui teste le format du resultat de l’appel à /api/fizzbuzz?fizzbuzznumber=10. 
Echec du test DONC refactor du controller pour faire passer le test. 
(A ce stade, juste un Mock de FizzBuzzService est utilisé sachant que la logique metier de ce Service sera testée séparément  
via la classe FizzBuzzServiceTest suivant la même approche TDD).

- Ecriture de la methode testFizzBuzzNumberValidation() qui teste les status code de réponse de l'API 
ainsi que les messages d’erreur pour les scenarios où le nombre en entrée n’est pas valide. 
Echec des tests puis mise en place au niveau du controller des validations et personnalisations de messages d’erreur necessaires pour faire passer les tests.

`Le Service FizzBuzzService`

- Création de la classe de Test FizzBuzzServiceTest

- Ecriture de la methode testGetListFromFizzBuzzNumberParam() qui teste si une Exception est levée lorsque le parametre 
de la methode getListFromFizzBuzzNumber(Integer) n’est pas élément de [1, 1000]. Le test Echoue DONC création de la classe FizzBuzzNumberException 
puis refactor de la methode du service pour integrer l’exception afin de faire passer le test. 
(Refactor du controller par la même occasion -> Ajout d’un block try catch lors de l’utilisation du service). 

- Enfin, écriture de la méthode testGetListFromFizzBuzzNumberReturn() qui teste les valeurs de retour valides pour le service. 
Refactor à chaque scenario pour implémenter la methode getListFromFizzBuzzNumber(Integer) du service et faire passer tous les tests.


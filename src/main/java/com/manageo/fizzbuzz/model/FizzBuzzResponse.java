package com.manageo.fizzbuzz.model;

import java.util.List;

public class FizzBuzzResponse {

    List<String> fizzbuzzResult;

    public FizzBuzzResponse(List<String> fizzbuzzResult) {
        this.fizzbuzzResult = fizzbuzzResult;
    }

    public List<String> getFizzbuzzResult() {
        return fizzbuzzResult;
    }

    public void setFizzbuzzResult(List<String> fizzbuzzResult) {
        this.fizzbuzzResult = fizzbuzzResult;
    }
}

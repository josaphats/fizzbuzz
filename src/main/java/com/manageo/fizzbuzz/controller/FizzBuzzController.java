package com.manageo.fizzbuzz.controller;

import com.manageo.fizzbuzz.config.SwaggerConfig;
import com.manageo.fizzbuzz.exception.FizzBuzzNumberException;
import com.manageo.fizzbuzz.model.FizzBuzzResponse;
import com.manageo.fizzbuzz.service.FizzBuzzService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.validation.ConstraintViolationException;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.HashSet;

@RestController
@RequestMapping("/api/fizzbuzz")
@Validated
@Api(tags = {SwaggerConfig.FIZZBUZZ_TAG})
public class FizzBuzzController {

    @Autowired
    FizzBuzzService fizzBuzzService;


    @ApiOperation(value = "retourne une fizzbuzz List pour un fizzbuzznumber donné", response = FizzBuzzResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Une liste fizzbuzzResult a été correctement retournée"),
            @ApiResponse(code = 400, message = "Le parametre fizzbuzznumber n'est pas valide")
        }
    )
    @RequestMapping(method = RequestMethod.GET, produces = {"application/json"})
    public ResponseEntity<FizzBuzzResponse> getListFromFizzBuzzNumber(
            @RequestParam(required = true)
            @Min(1)
            @Max(1000)
            @ApiParam(name = "fizzbuzznumber", required = true, value = "Entier compris entre 1 et 1000",
                    allowableValues = "range[1, 1000]") Integer fizzbuzznumber) {

        try {
            FizzBuzzResponse response = new FizzBuzzResponse(fizzBuzzService.getListFromFizzBuzzNumber(fizzbuzznumber));
            return ResponseEntity.ok(response);
        } catch (FizzBuzzNumberException e) {
            // lever cette Exception pour de passer la main à l'Exception Handler déclarée plus bas
            throw new ConstraintViolationException(
                    new HashSet<>());
        }

    }

    /* Utilisation d'ExceptionHandler pour limiter la personnalisation des messages d'erreur à ce Controller */
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<String> handleMissingServletRequestParameterException() {
        return ResponseEntity
                .badRequest()
                .body("pas de valeur pour le param fizzbuzznumber !!!");
    }
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<String> handleMethodArgumentTypeMismatchException() {
        return ResponseEntity
                .badRequest()
                .body("fizzbuzznumber doit etre un Entier !!!");
    }
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<String> handleConstraintViolationException() {
        return ResponseEntity
                .badRequest()
                .body("fizzbuzznumber doit etre compris entre 1 et 1000 !!!");
    }


}

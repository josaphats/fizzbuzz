package com.manageo.fizzbuzz.service;

import com.manageo.fizzbuzz.exception.FizzBuzzNumberException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FizzBuzzService {
    public List<String> getListFromFizzBuzzNumber(Integer fizzbuzzTestNumber) throws FizzBuzzNumberException {
        if (fizzbuzzTestNumber < 1 || fizzbuzzTestNumber > 1000) {
            throw new FizzBuzzNumberException("le parametre fizzbuzznumber doit etre compris entre 1 et 1000 !!!");
        }

        List<String> resultList = new ArrayList<>();
        for (int i = 0; i < fizzbuzzTestNumber; i++) {
            if((i+1)%3 == 0 && (i+1)%5 == 0) {
                resultList.add("FizzBuzz");
            } else if ((i+1)%3 == 0) {
                resultList.add("Fizz");
            } else if ((i+1)%5 == 0) {
                resultList.add("Buzz");
            } else {
                resultList.add(String.valueOf(i+1));
            }
        }

        return resultList;
    }
}

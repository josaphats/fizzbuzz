package com.manageo.fizzbuzz.exception;

public class FizzBuzzNumberException extends Exception {

    public FizzBuzzNumberException(String errorMessage) {
        super(errorMessage);
    }
}

package com.manageo.fizzbuzz.controller;

import com.manageo.fizzbuzz.service.FizzBuzzService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@AutoConfigureMockMvc
public class FizzBuzzControllerTest {

    /**
     * Cette classe de Test répond aux questions suivantes relatives à FizzBuzzController:
     *
     *  - l'API est-elle fonctionnelle et accessible ?
     *  - Est-ce que le résultat renvoyé par l'API est au format JSON ?
     *  - l'API traite t-elle correctement les scenarios où le nombre en entrée n'est pas valide ?
     */

    @Autowired
    MockMvc mockMvc;

    @MockBean
    FizzBuzzService fizzBuzzService;

    public static final int FIZZBUZZ_TEST_NUMBER = 10;
    public static final List<String> FIZZBUZZ_TEST_LIST = new ArrayList<>(Arrays.asList("1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz"));


    /* Scenario API fonctionnelle et accessible */
    @Test
    void testFizzBuzzEndpointAccessibility() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.get("/api/fizzbuzz")
                .queryParam("fizzbuzznumber", "100")
        ).andExpect(status().isOk());

    }

    /* Scenario le resultat de mon API est-il au Format JSON */
    @Test
    void testFizzBuzzEndpointReturnFormat() throws Exception {

        // Utilisation d'un Mock car le service FizzBuzzService sera implémenté ultérieurement via FizzBuzzServiceTest
        when(fizzBuzzService.getListFromFizzBuzzNumber(FIZZBUZZ_TEST_NUMBER)).thenReturn(FIZZBUZZ_TEST_LIST);

        // On verifie que le result est au format json et que le champs fizzbuzzResult est bien présent
        mockMvc.perform(MockMvcRequestBuilders.get("/api/fizzbuzz")
                .queryParam("fizzbuzznumber", "100")
        ).andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.fizzbuzzResult").isArray());

    }

    /* Scenario Validation du nombre reçu en entrée */
    @Test
    void testFizzBuzzNumberValidation() throws Exception {

        // Si pas de correcte query param, j'attends une 400 et un message explicite
        mockMvc.perform(MockMvcRequestBuilders.get("/api/fizzbuzz")
        ).andExpect(status().isBadRequest())
                .andExpect(content().string("pas de valeur pour le param fizzbuzznumber !!!"));

        mockMvc.perform(MockMvcRequestBuilders.get("/api/fizzbuzz")
                .queryParam("fizzbuzznumber", "")
        ).andExpect(status().isBadRequest())
                .andExpect(content().string("pas de valeur pour le param fizzbuzznumber !!!"));

        mockMvc.perform(MockMvcRequestBuilders.get("/api/fizzbuzz")
                .queryParam("autreparam", "20")
        ).andExpect(status().isBadRequest())
                .andExpect(content().string("pas de valeur pour le param fizzbuzznumber !!!"));


        // Si fizzbuzznumber n'est pas un Entier, j'attends une 400 et un message explicite
        mockMvc.perform(MockMvcRequestBuilders.get("/api/fizzbuzz")
                .queryParam("fizzbuzznumber", "fizzzz")
        ).andExpect(status().isBadRequest())
                .andExpect(content().string("fizzbuzznumber doit etre un Entier !!!"));

        mockMvc.perform(MockMvcRequestBuilders.get("/api/fizzbuzz")
                .queryParam("fizzbuzznumber", "50.7")
        ).andExpect(status().isBadRequest())
                .andExpect(content().string("fizzbuzznumber doit etre un Entier !!!"));
        mockMvc.perform(MockMvcRequestBuilders.get("/api/fizzbuzz")
                .queryParam("fizzbuzznumber", "50,7")
        ).andExpect(status().isBadRequest())
                .andExpect(content().string("fizzbuzznumber doit etre un Entier !!!"));

        // Si la valeur de fizzbuzznumber n'est pas comprise entre 1 et 1000, j'attends une 400 et message explicite
        mockMvc.perform(MockMvcRequestBuilders.get("/api/fizzbuzz")
                .queryParam("fizzbuzznumber", "-1")
        ).andExpect(status().isBadRequest())
                .andExpect(content().string("fizzbuzznumber doit etre compris entre 1 et 1000 !!!"));

        mockMvc.perform(MockMvcRequestBuilders.get("/api/fizzbuzz")
                .queryParam("fizzbuzznumber", String.valueOf(1001))
        ).andExpect(status().isBadRequest())
                .andExpect(content().string("fizzbuzznumber doit etre compris entre 1 et 1000 !!!"));
    }


}

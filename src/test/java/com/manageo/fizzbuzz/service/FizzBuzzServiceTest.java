package com.manageo.fizzbuzz.service;

import com.manageo.fizzbuzz.exception.FizzBuzzNumberException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class FizzBuzzServiceTest {

    /**
     * Cette classe répond aux questions suivantes relatives à la méthode getListFromFizzBuzzNumber de FizzBuzzService
     *
     *  - la methode valide t-elle correctement son parametre ?
     *  - la méthode renvoie t-elle une List et pas null ?
     *  - le contenu de la liste respecte t-il les règles métier specifiées dans l'enoncé de l'exercice ?
     */

    @Autowired
    FizzBuzzService fizzBuzzService;


    /* Scenario Validation du paramètre de la méthode getListFromFizzBuzzNumber  */
    @Test
    void testGetListFromFizzBuzzNumberParam() {

        // Si le parametre fizzbuzznumber n'est pas compris entre 1 et 1000, une exception doit etre levée
        Exception exception = assertThrows(FizzBuzzNumberException.class, () -> {
            fizzBuzzService.getListFromFizzBuzzNumber(0);
        });
        String expectedMessage = "le parametre fizzbuzznumber doit etre compris entre 1 et 1000 !!!";
        String exceptionMessage = exception.getMessage();
        assertTrue(exceptionMessage.contains(expectedMessage));
    }

    /* Scenario Validation du retour de la méthode getListFromFizzBuzzNumber  */
    @Test
    void testGetListFromFizzBuzzNumberReturn() throws FizzBuzzNumberException {
        // Parametre random valide pour getListFromFizzBuzzNumber
        int randomFizzBuzzNumber = (new Random()).nextInt(1000 - 1 + 1) + 1;

        List<String> fizzBuzzList =  fizzBuzzService.getListFromFizzBuzzNumber(randomFizzBuzzNumber);

        // La methode ne doit pas renvoyer null
        assertNotNull(fizzBuzzList);

        // La liste doit avoir au moins 1 élément
        assertTrue(fizzBuzzList.size() >= 1);

        // Le contenu de la liste doit respecter les règles metier de l'enoncé
        for (int i = 0; i < fizzBuzzList.size(); i++) {
            if((i+1)%3 == 0 && (i+1)%5 == 0) {
                assertEquals(fizzBuzzList.get(i), "FizzBuzz");
            } else if ((i+1)%3 == 0) {
                assertEquals(fizzBuzzList.get(i), "Fizz");
            } else if ((i+1)%5 == 0) {
                assertEquals(fizzBuzzList.get(i), "Buzz");
            } else {
                assertTrue(isInteger(fizzBuzzList.get(i)));
                assertEquals(Integer.parseInt(fizzBuzzList.get(i)), (i+1));
            }
        }
    }


    // Test si un string peut etre parsée en int
    public static boolean isInteger (String str) {
        if (str == null) {
            return false;
        }

        try {
            int parsedStr = Integer.parseInt(str);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

}
